# batch_crop_liv
what the&mdash;

if you are not my sister: this is intended to be a quick and dirty tool for my sister to use in a niche application. ymmv. but there are better tools out there.

if you are: i've already pierced through your seven proxies. my spells have triggered, and my super demons are moments away from your hideout. good luck.
(also don't worry about .gitignore or requirements.txt)

## Setting up
1. Download Python 3: https://www.python.org/downloads
    - Make sure you click the box that says "Add Python 3.whatever to PATH". Very important!
2. They said to run whatever fucking executable you downloaded or else I get it.
3. In the files you got from me, double-click setup.bat
    - Don't worry too much about the text on the window. I mostly left it there to help you know it's doing things rather than just hanging. It might take a couple minutes max.
    - When it's done, you can press any key to continue (as prompted) or just close the window.
4. thumbs fucking up.

## Batch-cropping images
1. Drag all input files into the INPUT folder
    - the only thing that should be in that folder is images. the program should be okay with it, but I didn't go too crazy trying to protect everything since it's just you using it. so who knows what happens lol
    - the program right now assumes that the images is at least 4,000 px wide and at least 3,000 px tall. i can make it handle more sizes if need be.
    - if you don't want to move them and you want to get silly, you can delete the INPUT folder, then make a symlink from the folder that holds the pictures to this folder and call it INPUT. i can walk you through that if you want
2. Double-click batch-crop-liv.bat

When it's done, you can press any key to continue (as prompted) or just close the window.

cropped copies appear in the OUTPUT folder. images in INPUT should not be touched. My computer did it in ~.2s per image in my testing so hopefully not too bad.

the program attempts to prevent itself from cropping the same image more than once to help save time. it's not very smart about it, though. I just check to see if I might have cropped the current input image before based on the filename, but it's not all that robust. If you see all this schtuff about skipping files, it might be a duplicate job, or the same filenames were used for different images. idk
