@ECHO OFF
ECHO This is a setup script. Once the window closes, you should be good to go.
ECHO You can ignore most the text you see here, including any warnings with weird colors.
ECHO _

ECHO Creating directories...
mkdir .\INPUT\
mkdir .\OUTPUT\
ECHO _

ECHO Upgrading installer...
python -m pip install --user --upgrade pip
ECHO _

ECHO Installing packages...
pip install -r requirements.txt
ECHO _

ECHO Done!
Pause
