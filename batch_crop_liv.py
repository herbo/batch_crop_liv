from PIL import Image
import glob, os, sys

# ASSUMPTIONS:
# all input images are 4000x3000 JPG

# = constants =

INPUT_RELPATH = './INPUT/'
OUTPUT_RELPATH = './OUTPUT/'
CROP_BOX = (0, 1500, 4000, 3000)

COMMENT = 'Original: {}\nCrop box: {}'

# = functions =

# input:
#   fn_in: the filename of an input image
#   fn_out: the filename for the cropped image
# makes a cropped copy, defined by CROP_BOX, of the image saved under fn_in and saves it as fn_out.
# returns True iff successfully cropped
def crop_single(fn_in, fn_out):
  # make sure output file doesn't already exist (lazy)
  if not os.path.isfile(fn_out):
    # no duplicate; crop

    try:
      img_in = Image.open(fn_in)
    except:
      print('  Cannot open image {}. Skipping.'.format(fn_in))
    else:
      comment_crop = COMMENT.format(fn_in, CROP_BOX)

      # crop, then save copy
      img_crop = img_in.crop(CROP_BOX)
      img_crop.save(fn_out, quality=95, comment=comment_crop)

      # close images
      img_crop.close()
      img_in.close()
      return True

  else:
    print('    WARNING: {} already cropped. Skipping to avoid duplicates\n'.format(fn_in))

  # only here if failed; return False
  return False


# = main =

if __name__ == '__main__':
  # TODO: support for non-default input, output paths, crop boxes, ...
  # TODO: validate crop box with image size

  is_verbose = False

  # parse command line
  if '-v' in sys.argv[1:]:
      is_verbose = True
      print('  Verbose on\n')

  num_cur = 1
  num_cropped = 0
  num_total = len(os.listdir(INPUT_RELPATH)) # lazy; iterate + os.path.isfile to filter ordinary files
  num_skipped = 0

  # loop over input images
  for fn_in in glob.iglob(INPUT_RELPATH + '*'):
    if is_verbose:
      print('  Processing {} ({}/{})...'.format(fn_in, num_cur, num_total))

    # make output filename
    f, e = os.path.splitext(os.path.basename(fn_in))
    fn_out = '{}{}_crop{}'.format(OUTPUT_RELPATH, f, e)

    # crop image
    if crop_single(fn_in, fn_out):
      num_cropped += 1
    else:
      num_skipped += 1

    num_cur += 1

  print('Cropped {} of {} images ({} skipped).'.format(num_cropped, num_total, num_skipped))
